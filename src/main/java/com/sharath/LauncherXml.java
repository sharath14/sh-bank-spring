package com.sharath;

import com.sharath.bank.orchestrator.CmdLineOrchestrator;
import com.sharath.bank.orchestrator.CmdLineOrchestratorImpl;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LauncherXml
{
    public static void main( String[] args )
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bankAppContext.xml");
        CmdLineOrchestrator orchestrator = context.getBean("orchestrator", CmdLineOrchestrator.class);
        orchestrator.runTheBusinessFlow();
        context.close();
    }
}
