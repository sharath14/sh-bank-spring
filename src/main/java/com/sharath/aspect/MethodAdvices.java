package com.sharath.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MethodAdvices {
    static org.slf4j.Logger logger = LoggerFactory.getLogger(MethodAdvices.class);

   @Before("execution(* com.sharath.bank.orchestrator.CmdLineOrchestratorImpl.runTheBusinessFlow(..))")
    public void doAspectCheck(JoinPoint joinPoint) {
        logger.info("doing Aspect check");
    }

    @After("execution(* com.sharath.bank.orchestrator.CmdLineOrchestratorImpl.runTheBusinessFlow(..))")
    public void endOfAspect(JoinPoint joinPoint) {
       logger.info("End of Aspect check");
    }
}
