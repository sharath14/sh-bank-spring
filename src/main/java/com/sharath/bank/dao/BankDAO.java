package com.sharath.bank.dao;

import com.sharath.bank.model.Account;
import com.sharath.bank.model.Customer;

public interface BankDAO {
    Account insertAccount(Account account);

    Customer insertCustomer(Customer customer);

    Boolean updateAccountBalance(Account account, Double money);

}
