package com.sharath.bank.dao;

import com.sharath.bank.model.Account;
import com.sharath.bank.model.Customer;
import com.sharath.bank.orchestrator.CmdLineOrchestratorImpl;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BankDataBaseDAOImpl implements BankDAO{
    static org.slf4j.Logger logger = LoggerFactory.getLogger(BankDataBaseDAOImpl.class);
    @Override
    public Account insertAccount(Account account) {
        logger.info("inserted account info");
        return null;
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        logger.info("inserted customer info");
        return null;
    }

    @Override
    public Boolean updateAccountBalance(Account account, Double money) {
        logger.info("Updated account balance");
        return  null;
    }
}
