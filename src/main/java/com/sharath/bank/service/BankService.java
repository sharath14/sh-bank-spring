package com.sharath.bank.service;

import com.sharath.bank.model.Account;
import com.sharath.bank.model.Customer;
import com.sharath.bank.model.DomainObject;

public interface BankService  {
    Account createAccount(Account account);

    Customer createCustomer(Customer customer);

    Boolean depositMoney(Account account, Double money);
}
