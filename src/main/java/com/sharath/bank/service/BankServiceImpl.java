package com.sharath.bank.service;

import com.sharath.bank.dao.BankDAO;
import com.sharath.bank.dao.BankDataBaseDAOImpl;
import com.sharath.bank.model.Account;
import com.sharath.bank.model.Customer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class BankServiceImpl implements BankService {
    static org.slf4j.Logger logger = LoggerFactory.getLogger(BankServiceImpl.class);

    @Autowired
    private BankDAO bankDAO;


    @Override
    public Account createAccount(Account account) {
        logger.info("created account");
        bankDAO.insertAccount(account);
        return new Account();
    }

    @Override
    public Customer createCustomer(Customer customer) {
        logger.info("created customer");
        bankDAO.insertCustomer(customer);
        return new Customer();
    }

    @Override
    public Boolean depositMoney(Account account, Double money) {
        logger.info("deposited money");
        bankDAO.updateAccountBalance(account, money);
        return Boolean.TRUE;
    }

    public void setBankDAO(BankDAO bankDAO) {
        this.bankDAO = bankDAO;
    }
}
