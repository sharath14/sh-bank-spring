package com.sharath.bank.orchestrator;

import com.sharath.bank.frontend.AccountCmdLineInputCollector;
import com.sharath.bank.frontend.CustomerCmdLineInputCollector;
import com.sharath.bank.frontend.InputCollector;
import com.sharath.bank.model.Account;
import com.sharath.bank.model.Customer;
import com.sharath.bank.service.BankServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("cmdLineOrchestrator")
public class CmdLineOrchestratorImpl implements CmdLineOrchestrator {
    static org.slf4j.Logger logger = LoggerFactory.getLogger(CmdLineOrchestratorImpl.class);


    @Autowired
    private BankServiceImpl bankService;

    @Autowired
    private AccountCmdLineInputCollector accCmdLnIp;

    @Autowired
    private CustomerCmdLineInputCollector cusCmdLnIp;

    private Double money;

    public void setBankService(BankServiceImpl bankService) {
        this.bankService = bankService;
    }

    public void setAccCmdLnIp(AccountCmdLineInputCollector accCmdLnIp) {
        this.accCmdLnIp = accCmdLnIp;
    }

    public void setCusCmdLnIp(CustomerCmdLineInputCollector cusCmdLnIp) {
        this.cusCmdLnIp = cusCmdLnIp;
    }

    @Override
    public void runTheBusinessFlow() {
        logger.info("running the business flow");
        Account accountInfo = accCmdLnIp.collectingUserInput();
        Customer customerInfo = cusCmdLnIp.collectingUserInput();

        bankService.createAccount(accountInfo);
        bankService.createCustomer(customerInfo);
        bankService.depositMoney(accountInfo,money);
    }
}
