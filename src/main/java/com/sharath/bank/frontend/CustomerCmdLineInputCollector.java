package com.sharath.bank.frontend;

import com.sharath.bank.model.Customer;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CustomerCmdLineInputCollector implements InputCollector<Customer> {
    static org.slf4j.Logger logger = LoggerFactory.getLogger(CustomerCmdLineInputCollector.class);

    @Override
    public Customer collectingUserInput() {
        logger.info("collected customer info");
        return null;
    }
}
