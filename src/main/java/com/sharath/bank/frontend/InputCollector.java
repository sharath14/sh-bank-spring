package com.sharath.bank.frontend;

import com.sharath.bank.model.DomainObject;

public interface InputCollector<T extends DomainObject>{
    T collectingUserInput();
}
