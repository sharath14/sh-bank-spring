package com.sharath.bank.frontend;

import com.sharath.bank.model.Account;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountCmdLineInputCollector implements InputCollector<Account> {
    static org.slf4j.Logger logger = LoggerFactory.getLogger(AccountCmdLineInputCollector.class);
    @Override
    public Account collectingUserInput() {
        logger.info("collecting account info");
        return null;

    }
}
