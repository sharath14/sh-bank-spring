package com.sharath;

import com.sharath.bank.orchestrator.CmdLineOrchestrator;
import com.sharath.bank.orchestrator.CmdLineOrchestratorImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class LauncherAnnotations
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext configApplicationContext =
                new AnnotationConfigApplicationContext("com.sharath");

        CmdLineOrchestrator cmdLineOrchestrator =
                 configApplicationContext.getBean("cmdLineOrchestrator", CmdLineOrchestrator.class);

        cmdLineOrchestrator.runTheBusinessFlow();
        configApplicationContext.close();
    }
}
